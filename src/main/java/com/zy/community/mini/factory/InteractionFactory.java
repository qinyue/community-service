package com.zy.community.mini.factory;

import com.zy.community.community.domain.ZyComment;
import com.zy.community.community.domain.ZyCommunityInteraction;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.framework.security.mini.MiniContextUtils;
import com.zy.community.web.controller.mini.community.dto.CommentRequestDto;
import com.zy.community.web.controller.mini.community.dto.CommunityInteractionDto;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class InteractionFactory {
    @Resource
    private ZyOwnerMapper zyOwnerMapper;

    public ZyCommunityInteraction createByDto(CommunityInteractionDto dto){
        if (dto==null) return null;
        ZyCommunityInteraction zyCommunityInteraction = new ZyCommunityInteraction();
        zyCommunityInteraction.setContent(dto.getContent());
        zyCommunityInteraction.setUserId(zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId()));
        zyCommunityInteraction.setCreateTime(new Date());
        zyCommunityInteraction.setCommunityId(dto.getCommunityId());
        return zyCommunityInteraction;
    }

    public ZyComment createComment(CommentRequestDto dto){
        if (dto==null) return null;
        ZyComment comment = new ZyComment();
        comment.setUserId(zyOwnerMapper.findOwnerIdByOpenId(MiniContextUtils.getOpenId()));
        comment.setContent(dto.getContent());
        comment.setInteractionId(dto.getInteractionId());
        comment.setParentId(dto.getParentId());
        comment.setCreateTime(new Date());
        comment.setRootId(dto.getRootId());
        return comment;
    }






}
