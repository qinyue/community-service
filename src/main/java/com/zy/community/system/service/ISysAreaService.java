package com.zy.community.system.service;

import com.zy.community.system.dto.AreaDto;
import java.util.List;

/**
 * @author yangdi
 */
public interface ISysAreaService {
    /**
     * @return 获取区域整树
     */
    List<AreaDto> findRootArea();
}
