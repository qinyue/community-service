package com.zy.community.community.domain;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 文件管理对象 zy_files
 * 
 * @author yin
 * @date 2020-12-17
 */
public class ZyFiles extends BaseEntity
{

    /** 文件ID */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long filesId;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String filesUrl;

    /** 删除状态0默认1删除 */
    private Integer delFlag;

    /** 来源0APP端，1PC端 */
    @Excel(name = "来源0APP端，1PC端")
    private Integer source;

    /**创建人ID*/
    private Long userId;

    /**父级ID*/
    private Long parentId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setFilesId(Long filesId)
    {
        this.filesId = filesId;
    }

    public Long getFilesId() 
    {
        return filesId;
    }
    public void setFilesUrl(String filesUrl) 
    {
        this.filesUrl = filesUrl;
    }

    public String getFilesUrl() 
    {
        return filesUrl;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }
    public void setSource(Integer source) 
    {
        this.source = source;
    }

    public Integer getSource() 
    {
        return source;
    }

    @Override
    public String toString() {
        return "ZyFiles{" +
                "filesId=" + filesId +
                ", filesUrl='" + filesUrl + '\'' +
                ", delFlag=" + delFlag +
                ", source=" + source +
                ", userId=" + userId +
                ", parentId=" + parentId +
                '}';
    }
}
