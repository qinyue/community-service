package com.zy.community.community.domain;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 评论对象 zy_comment
 * 
 * @author yin
 * @date 2020-12-18
 */
public class ZyComment extends BaseEntity
{

    /** id */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long commentId;

    /**
     * 评论的划属Id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long rootId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 父级ID */
    @Excel(name = "父级ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long parentId;

    /** 删除状态0默认1删除 */
    private Integer delFlag;

    /** 社区互动ID */
    @Excel(name = "社区互动ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long interactionId;

    /**创建人ID*/
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setCommentId(Long commentId)
    {
        this.commentId = commentId;
    }

    public Long getCommentId() 
    {
        return commentId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }
    public void setInteractionId(Long interactionId) 
    {
        this.interactionId = interactionId;
    }

    public Long getInteractionId() 
    {
        return interactionId;
    }

    public Long getRootId() {
        return rootId;
    }

    public void setRootId(Long rootId) {
        this.rootId = rootId;
    }

    @Override
    public String toString() {
        return "ZyComment{" +
                "commentId=" + commentId +
                ", content='" + content + '\'' +
                ", parentId=" + parentId +
                ", delFlag=" + delFlag +
                ", interactionId=" + interactionId +
                ", userId=" + userId +
                '}';
    }
}
