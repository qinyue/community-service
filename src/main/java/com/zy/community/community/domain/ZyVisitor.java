package com.zy.community.community.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 访客邀请 对象 zy_visitor
 *
 * @author yangdi
 * @date 2020-12-18
 */
public class ZyVisitor extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long visitorId;

    /**
     * 小区id
     */
    @Excel(name = "小区id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /**
     * 访客姓名
     */
    @Excel(name = "访客姓名")
    private String visitorName;

    /**
     * 访客手机号
     */
    @Excel(name = "访客手机号")
    private String visitorPhoneNumber;

    /**
     * 到访时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到访时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date visitorDate;

    /**
     * 创建人id
     */
    @Excel(name = "创建人id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long createById;

    /**
     * 创建人openid
     */
    @Excel(name = "创建人openid")
    private String createByOpenId;

    public void setVisitorId(Long visitorId) {
        this.visitorId = visitorId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getVisitorId() {
        return visitorId;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorPhoneNumber(String visitorPhoneNumber) {
        this.visitorPhoneNumber = visitorPhoneNumber;
    }

    public String getVisitorPhoneNumber() {
        return visitorPhoneNumber;
    }

    public void setVisitorDate(Date visitorDate) {
        this.visitorDate = visitorDate;
    }

    public Date getVisitorDate() {
        return visitorDate;
    }

    public void setCreateById(Long createById) {
        this.createById = createById;
    }

    public Long getCreateById() {
        return createById;
    }

    public void setCreateByOpenId(String createByOpenId) {
        this.createByOpenId = createByOpenId;
    }

    public String getCreateByOpenId() {
        return createByOpenId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("communityId", getCommunityId())
                .append("visitorId", getVisitorId())
                .append("visitorName", getVisitorName())
                .append("visitorPhoneNumber", getVisitorPhoneNumber())
                .append("visitorDate", getVisitorDate())
                .append("createById", getCreateById())
                .append("createByOpenId", getCreateByOpenId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
