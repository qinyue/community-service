package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyComplaintSuggest;
import com.zy.community.community.domain.dto.ZyComplaintSuggestDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 投诉建议 Mapper接口
 *
 * @author yangdi
 * @date 2020-12-18
 */
public interface ZyComplaintSuggestMapper extends BaseMapper<ZyComplaintSuggest> {

    /**
     * 查询列表
     * @param zyComplaintSuggest
     * @return
     */
    @Select("<script>" +
            "select a.complaint_suggest_id,a.community_id, a.complaint_suggest_type," +
            " a.complaint_suggest_content, a.create_by, a.create_time, a.update_by," +
            " a.update_time, a.remark, b.owner_nickname as ownerNickname," +
            " b.owner_real_name as ownerRealName,b.owner_phone_number as ownerPhoneNumber" +
            " from zy_complaint_suggest a LEFT JOIN zy_owner b on a.user_id = b.owner_id " +
            "<where>" +
            "<if test=\"complaintSuggestType != null\"> and complaint_suggest_type = #{complaintSuggestType}</if>" +
            "and a.community_id = #{communityId}" +
            "</where>" +
            " order by a.create_time DESC"+
            "</script>")
    List<ZyComplaintSuggestDto> selectZyComplaintSuggestList(ZyComplaintSuggest zyComplaintSuggest);
}
