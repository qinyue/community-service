package com.zy.community.community.service;

import com.zy.community.community.domain.ZyOwnerRoom;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.dto.ZyOwnerRoomDto;
import com.zy.community.community.domain.dto.ZyOwnerRoomRecordDto;

import java.util.List;

/**
 * 房屋绑定 Service接口
 *
 * @author yangdi
 * @date 2020-12-15
 */
public interface IZyOwnerRoomService {

    /**
     * 查询房屋绑定 列表
     *
     * @param zyOwnerRoom 房屋绑定
     * @return 房屋绑定 集合
     */
    public List<ZyOwnerRoomDto> selectZyOwnerRoomList(ZyOwnerRoom zyOwnerRoom);

    /**
     * 修改房屋绑定
     *
     * @param zyOwnerRoomRecord 房屋绑定
     * @return 结果
     */
    public int updateZyOwnerRoom(ZyOwnerRoomRecord zyOwnerRoomRecord);

    /**
     * 查询审核记录
     * @param ownerRoomId
     * @return
     */
    List<ZyOwnerRoomRecordDto> queryOwnerRoomRecordByOwnerRoomId(Long ownerRoomId);
}
