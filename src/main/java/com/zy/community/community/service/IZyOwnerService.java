package com.zy.community.community.service;

import com.zy.community.community.domain.ZyOwner;
import com.zy.community.community.domain.dto.ZyOwnerDto;

import java.util.List;

/**
 * 业主 Service接口
 * 
 * @author yangdi
 * @date 2020-12-16
 */
public interface IZyOwnerService 
{
    /**
     * 查询业主 列表
     * 
     * @param zyOwner 业主 
     * @return 业主 集合
     */
    public List<ZyOwnerDto> selectZyOwnerList(ZyOwner zyOwner);

    /**
     * 解绑
     * @param ownerRoomId
     * @return
     */
    int deleteZyOwnerByIds(Long ownerRoomId);
}
