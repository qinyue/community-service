package com.zy.community.community.service;
import com.zy.community.community.domain.ZyFiles;

import java.util.List;

/**
 * 文件管理Service接口
 * 
 * @author yin
 * @date 2020-12-17
 */
public interface IZyFilesService 
{
    /**
     * 查询文件管理
     * 
     * @param filesId 文件管理ID
     * @return 文件管理
     */
    public ZyFiles selectZyFilesById(Long filesId);

    /**
     * 查询文件管理列表
     * 
     * @param zyFiles 文件管理
     * @return 文件管理集合
     */
    public List<ZyFiles> selectZyFilesList(ZyFiles zyFiles);

    /**
     * 新增文件管理
     * 
     * @param zyFiles 文件管理
     * @return 结果
     */
    public int insertZyFiles(ZyFiles[] zyFiles);

    /**
     * 修改文件管理
     *
     * @param zyFiles 文件管理
     * @return 结果
     */
    public int updateZyFiles(ZyFiles zyFiles);

    /**
     * 批量删除文件管理
     * 
     * @param filesIds 需要删除的文件管理ID
     * @return 结果
     */
    public int deleteZyFilesByIds(Long[] filesIds);

    /**
     * 删除文件管理信息
     * 
     * @param filesId 文件管理ID
     * @return 结果
     */
    public int deleteZyFilesById(Long filesId);

    /**
     * 根据fileIds获取图片列表
     * @param parentId
     * @return
     */
    List getWyglFilesList(Long parentId);
}
