package com.zy.community.community.service.impl;

import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.exception.CustomException;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.common.utils.ServletUtils;
import com.zy.community.community.domain.ZyRoom;
import com.zy.community.community.domain.dto.ZyRoomDto;
import com.zy.community.community.mapper.ZyRoomMapper;
import com.zy.community.community.service.IZyRoomService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 房间 Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-11
 */
@Service
public class IZyRoomServiceImpl implements IZyRoomService {
    @Value("${community.value}")
    private String propertyId;
    @Resource
    private ZyRoomMapper zyRoomMapper;
    private static final String CODE_PREFIX = "ROOM_";

    /**
     * 查询房间
     *
     * @param roomId 房间 ID
     * @return 房间
     */
    @Override
    public ZyRoom selectZyRoomById(Long roomId) {
        return zyRoomMapper.selectById(roomId);
    }

    /**
     * 查询房间 列表
     *
     * @param zyRoom 房间
     * @return 房间
     */
    @Override
    public List<ZyRoomDto> selectZyRoomList(ZyRoom zyRoom) {

        zyRoom.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));

        return zyRoomMapper.selectZyRoomList(zyRoom);
    }

    /**
     * 新增房间
     *
     * @param zyRoom 房间
     * @return 结果
     */
    @Override
    public int insertZyRoom(ZyRoom zyRoom) {

        zyRoom.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));

        zyRoom.setRoomCode(CODE_PREFIX+System.currentTimeMillis());
        ZyRoom zyRoomOne = zyRoomMapper.selectOne(new QueryWrapper<>(new ZyRoom() {
            {
                setCommunityId(zyRoom.getCommunityId());
                setBuildingId(zyRoom.getBuildingId());
                setUnitId(zyRoom.getUnitId());
                setRoomName(zyRoom.getRoomName());
            }
        }));
        if(zyRoomOne != null) {
            throw new CustomException("该单元已有添加房间，请检查后重新提交", HttpStatus.CONFLICT);
        }
        return zyRoomMapper.insert(ManageParametersUtils.saveMethod(zyRoom));
    }

    /**
     * 修改房间
     *
     * @param zyRoom 房间
     * @return 结果
     */
    @Override
    public int updateZyRoom(ZyRoom zyRoom) {
        return zyRoomMapper.updateById(ManageParametersUtils.updateMethod(zyRoom));
    }

    /**
     * 批量删除房间
     *
     * @param roomIds 需要删除的房间 ID
     * @return 结果
     */
    @Override
    public int deleteZyRoomByIds(Long[] roomIds) {
        return zyRoomMapper.deleteBatchIds(Arrays.asList(roomIds));
    }

    /**
     * 删除房间 信息
     *
     * @param roomId 房间 ID
     * @return 结果
     */
    @Override
    public int deleteZyRoomById(Long roomId) {
        return zyRoomMapper.deleteById(roomId);
    }
}
