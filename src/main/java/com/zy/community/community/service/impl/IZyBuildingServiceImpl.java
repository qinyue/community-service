package com.zy.community.community.service.impl;

import java.util.Arrays;
import java.util.List;

import com.zy.community.common.annotation.DataScope;
import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.exception.CustomException;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.common.utils.ServletUtils;
import com.zy.community.community.domain.ZyBuilding;
import com.zy.community.community.domain.dto.KnDto;
import com.zy.community.community.domain.dto.ZyBuildingDto;
import com.zy.community.community.mapper.ZyBuildingMapper;
import com.zy.community.community.service.IZyBuildingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 楼栋 Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-11
 */
@Service
public class IZyBuildingServiceImpl implements IZyBuildingService {
    @Value("${community.value}")
    private String propertyId;

    @Resource
    private ZyBuildingMapper zyBuildingMapper;

    private static final String CODE_PREFIX = "BUILDING_";

    /**
     * 查询楼栋
     *
     * @param buildingId 楼栋 ID
     * @return 楼栋
     */
    @Override
    public ZyBuilding selectZyBuildingById(Long buildingId) {
        return zyBuildingMapper.selectById(buildingId);
    }

    /**
     * 查询楼栋 列表
     *
     * @param zyBuilding 楼栋
     * @return 楼栋
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<ZyBuildingDto> selectZyBuildingList(ZyBuilding zyBuilding) {
        zyBuilding.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));
        return zyBuildingMapper.selectZyBuildingList(zyBuilding);
    }

    /**
     * 新增楼栋
     *
     * @param zyBuilding 楼栋
     * @return 结果
     */
    @Override
    public int insertZyBuilding(ZyBuilding zyBuilding) {

        zyBuilding.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));
        zyBuilding.setBuildingCode(CODE_PREFIX+System.currentTimeMillis());
        return zyBuildingMapper.insert(ManageParametersUtils.saveMethod(zyBuilding));
    }

    /**
     * 修改楼栋
     *
     * @param zyBuilding 楼栋
     * @return 结果
     */
    @Override
    public int updateZyBuilding(ZyBuilding zyBuilding) {
        zyBuilding.setUpdateTime(DateUtils.getNowDate());
        return zyBuildingMapper.updateById(zyBuilding);
    }

    /**
     * 批量删除楼栋
     *
     * @param buildingIds 需要删除的楼栋 ID
     * @return 结果
     */
    @Override
    public int deleteZyBuildingByIds(Long[] buildingIds) {
        return zyBuildingMapper.deleteBatchIds(Arrays.asList(buildingIds));
    }

    /**
     * 删除楼栋 信息
     *
     * @param buildingId 楼栋 ID
     * @return 结果
     */
    @Override
    public int deleteZyBuildingById(Long buildingId) {
        return zyBuildingMapper.deleteById(buildingId);
    }

    /**
     * 房间新增所需下拉
     * @param zyBuilding
     * @return
     */
    @DataScope(deptAlias = "a")
    @Override
    public List<KnDto> queryPullDownRoom(ZyBuilding zyBuilding) {

        zyBuilding.setCommunityId(RequestHeaderUtils.GetCommunityId(propertyId));

        return zyBuildingMapper.queryPullDownRoom(zyBuilding);
    }
}
