package com.zy.community.framework.security.handle;

import com.alibaba.fastjson.JSON;
import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.utils.ServletUtils;
import com.zy.community.common.utils.StringUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.zip.ZipEntry;

/**
 * 认证失败处理类 返回未授权
 * 
 * @author yangdi
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable
{
    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException
    {
        int code = HttpStatus.UNAUTHORIZED;
        String msg = StringUtils.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
        ServletUtils.renderString(response, JSON.toJSONString(ZyResult.fail(code, msg)));
    }
}
