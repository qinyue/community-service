package com.zy.community.web.controller.community;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyComment;
import com.zy.community.community.domain.dto.ZyCommentDto;
import com.zy.community.community.service.IZyCommentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 评论Controller
 * 
 * @author yin
 * @date 2020-12-18
 */
@Api(tags = "互动评论")
@RestController
@RequestMapping("/mini/comment")
public class ZyCommentController extends BaseController
{
    @Autowired
    private IZyCommentService zyCommentService;

    /**
     * 查询评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyCommentDto zyComment)
    {
        startPage();
        List<ZyCommentDto> list = zyCommentService.selectZyCommentList(zyComment);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @PreAuthorize("@ss.hasPermi('system:comment:export')")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyCommentDto zyComment)
    {
        List<ZyCommentDto> list = zyCommentService.selectZyCommentList(zyComment);
        ExcelUtil<ZyCommentDto> util = new ExcelUtil<ZyCommentDto>(ZyCommentDto.class);
        return util.exportExcel(list, "comment");
    }

    /**
     * 获取评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:comment:query')")
    @GetMapping(value = "/{commentId}")
    public ZyResult<ZyComment> getInfo(@PathVariable("commentId") Long commentId)
    {
        return ZyResult.data(zyCommentService.selectZyCommentById(commentId));
    }

    /**
     * 新增评论
     */
    @PreAuthorize("@ss.hasPermi('system:comment:add')")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyComment zyComment)
    {
        return toZyAjax(zyCommentService.insertZyComment(zyComment));
    }

    /**
     * 修改评论
     */
    @PreAuthorize("@ss.hasPermi('system:comment:edit')")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyComment zyComment)
    {
        return toZyAjax(zyCommentService.updateZyComment(zyComment));
    }

    /**
     * 删除评论
     */
    @PreAuthorize("@ss.hasPermi('system:comment:remove')")
    @Log(title = "评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{commentIds}")
    public ZyResult remove(@PathVariable Long[] commentIds)
    {
        return toZyAjax(zyCommentService.deleteZyCommentByIds(commentIds));
    }
}
