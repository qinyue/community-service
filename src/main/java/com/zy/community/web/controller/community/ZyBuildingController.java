package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyBuilding;
import com.zy.community.community.domain.dto.KnDto;
import com.zy.community.community.domain.dto.ZyBuildingDto;
import com.zy.community.community.service.IZyBuildingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 楼栋 Controller
 *
 * @author ruoyi
 * @date 2020-12-11
 */
@Api(tags = "楼栋")
@RestController
@RequestMapping("/system/building")
public class ZyBuildingController extends BaseController {
    @Resource
    private IZyBuildingService zyBuildingService;

    /**
     * 查询楼栋 列表
     */
    @ApiOperation(value = "查询楼栋")
    @PreAuthorize("@ss.hasPermi('system:building:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyBuilding zyBuilding) {
        startPage();
        List<ZyBuildingDto> list = zyBuildingService.selectZyBuildingList(zyBuilding);
        return getDataTable(list);
    }

    /**
     * 下拉 列表
     */
    @ApiOperation(value = "下拉")
    @GetMapping("/queryPullDown")
    public ZyResult queryPullDown(ZyBuilding zyBuilding) {
        List<ZyBuildingDto> list = zyBuildingService.selectZyBuildingList(zyBuilding);
        return ZyResult.data(list);
    }

    /**
     * 房间新增所需下拉 列表
     */
    @ApiOperation(value = "房间新增所需下拉")
    @GetMapping("/queryPullDownRoom")
    public ZyResult queryPullDownRoom(ZyBuilding zyBuilding) {
        List<KnDto> list = zyBuildingService.queryPullDownRoom(zyBuilding);
        return ZyResult.data(list);
    }


    /**
     * 导出楼栋 列表
     */
    @ApiOperation(value = "导出楼栋")
    @PreAuthorize("@ss.hasPermi('system:building:export')")
    @Log(title = "楼栋 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyBuilding zyBuilding) {
        List<ZyBuildingDto> list = zyBuildingService.selectZyBuildingList(zyBuilding);
        ExcelUtil<ZyBuildingDto> util = new ExcelUtil<ZyBuildingDto>(ZyBuildingDto.class);
        return util.exportExcel(list, "building");
    }

    /**
     * 获取楼栋 详细信息
     */
    @ApiOperation(value = "获取楼栋")
    @PreAuthorize("@ss.hasPermi('system:building:query')")
    @GetMapping(value = "/{buildingId}")
    public ZyResult getInfo(@PathVariable("buildingId") Long buildingId) {
        return ZyResult.data(zyBuildingService.selectZyBuildingById(buildingId));
    }

    /**
     * 新增楼栋
     */
    @ApiOperation(value = "新增楼栋")
    @PreAuthorize("@ss.hasPermi('system:building:add')")
    @Log(title = "楼栋 ", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyBuilding zyBuilding) {
        return toAjax(zyBuildingService.insertZyBuilding(zyBuilding));
    }

    /**
     * 修改楼栋
     */
    @ApiOperation(value = "修改楼栋")
    @PreAuthorize("@ss.hasPermi('system:building:edit')")
    @Log(title = "楼栋 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyBuilding zyBuilding) {
        return toAjax(zyBuildingService.updateZyBuilding(zyBuilding));
    }

    /**
     * 删除楼栋
     */
    @ApiOperation(value = "删除楼栋")
    @PreAuthorize("@ss.hasPermi('system:building:remove')")
    @Log(title = "楼栋 ", businessType = BusinessType.DELETE)
    @DeleteMapping("/{buildingIds}")
    public ZyResult remove(@PathVariable Long[] buildingIds) {
        return toAjax(zyBuildingService.deleteZyBuildingByIds(buildingIds));
    }
}
