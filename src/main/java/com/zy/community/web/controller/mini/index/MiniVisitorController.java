package com.zy.community.web.controller.mini.index;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageInfo;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.index.MiniVisitorService;
import com.zy.community.web.controller.mini.index.dto.VisitorDto;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 访客的Api
 */
@RestController
@RequestMapping("/mini/community/visitor")
public class MiniVisitorController {
    @Resource
    private MiniVisitorService miniVisitorService;

    /**
     * 保存访客邀请记录
     * @param visitorDto 访客邀请dto
     * @param bindingResult 异常结果
     * @return 结果
     */
    @PostMapping("/save")
    public ZyResult<String> saveVisitor(@RequestBody @Valid VisitorDto visitorDto, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return ZyResult.fail(400,bindingResult.getFieldError().getDefaultMessage());
        }
        return miniVisitorService.saveVisitor(visitorDto);
    }

    /**
     * 查询最新的3位到访人
     * @return 到访人列表
     */
    @GetMapping("/top3/{communityId}")
    public ZyResult<List<VisitorDto>> findLatest3Visitor(@PathVariable("communityId") Long communityId){
        return miniVisitorService.findLatest3Visitor(communityId);
    }
    @GetMapping("/page")
    public ZyResult<PageInfo<VisitorDto>> findVisitorPage(@RequestParam Long communityId,
                                                          @RequestParam Integer size,
                                                          @RequestParam Integer current){
        return miniVisitorService.findVisitorsByPage(size,current,communityId);
    }
}
