package com.zy.community.common.enums;

/**
 * 数据源
 * 
 * @author yangdi
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
